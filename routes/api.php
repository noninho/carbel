<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
// Rota para detalhes do veículo
Route::get('/veiculo/detalhes/{slug}', 'VeiculoController@detalhesVeiculo')->name('detalhesVeiculo');

// Rota para buscar os veículos
Route::get('/{tipoVeiculo}/{filtro1?}/{filtro2?}/{filtro3?}/{filtro4?}/{filtro5?}/{filtro6?}', 'CrawlController@crawl')->name('crawl');

