<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Nunito', sans-serif;
                font-weight: 200;
                height: 100vh;
                margin: 0;
            }

            .position-ref {
                position: relative;
            }

            .titulo {
                text-align: center;
                width:100%;
            }

            .readme {
                text-align: left;
                width:100%;
                padding: 10px;
            }

            .title {
                font-size: 30px;
                margin-top:15px;
            }

            .m-b-md {
                margin-bottom: 10px;
            }

            .detalhes {
                margin-left: 30px;
            }

            .titulo {
                margin-top: 20px;
            }
        </style>
    </head>
    <body>
        <div class="flex-center position-ref">

            <div class="titulo">
                <div class="title m-b-md">
                    Teste Carbel
                </div>

            </div>
            
            <div class="readme">
                <h2>Readme</h2>

                <p>
                    Clone do repositório: <br>
                    git clone https://noninho@bitbucket.org/noninho/carbel.git
                    
                    <br>
                    <br>
                    
                    Rodar o composer install: <br>
                    composer install
                </p>

                <p>
                    <h5>Crawl de veículos</h5>
                    Para usar o endpoint de busca de veículos ultilize a url: <br>
                    <strong>/veiculo/marca/modelo/ano-ANO_MIN-ANO_MAX/preco-PRECO_MIN-PRECO_MAX/km-KM_MIN-KM_MAX</strong>
                </p>

                <p class="detalhes">
                    onde: <br>
                    <strong>veiculo(obrigatório)</strong>: carro|moto|caminhao <br>
                    <strong>marca</strong>: marca do veículo. ex: honda, volkswagen <br>
                    <strong>modelo</strong>: modelo do veículo. ex: jetta, golf <br>
                    <strong>ANO_MIN</strong>: Ano mínimo com 4 digitos <br>
                    <strong>ANO_MAX</strong>: Ano máximo com 4 digitos <br>
                    <strong>PRECO_MIN</strong>: preço minimo <br>
                    <strong>PRECO_MAX</strong>: preço máximo <br>
                    <strong>KM_MIN</strong>: kilometragem mínima <br>
                    <strong>KM_MAX</strong>: kilometragem máxima <br>
                    <br>
                    <strong>ex1</strong>: http://localhost/api/carro/volkswagen/ano--2012/preco-50000-80000
                    <br>
                    <strong>ex2</strong>: http://localhost/api/carro/honda/civic/ano-2008-2012/preco-20000-
                </p>

                <p>
                    <h3>Detalhes de veículos</h3>
                    Para usar o endpoint de detalhes do veículo ultilize a url: <br>
                    <strong>/detalhes/{slug}</strong> <br>
                    O slug deve ser o mesmo de algum veiculo do site: https://seminovos.com.br, por exemplo:
                    <br>
                    <p class="detalhes">
                        <strong>ex1</strong>: http://localhost/api/veiculo/detalhes/chevrolet-onix-hatch-2015--2594992
                        <br>
                        <strong>ex2</strong>: http://localhost/api/veiculo/detalhes/nissan-frontier-xl-2.5-8v-4portas-2012-2013--2501483
                    </p>
                </p>

                <p>
                    <h3>Considerações</h3>
                    <strong>PS</strong> Desculpe a falta de capricho, a chuva acabou com a luz na minha cidade e tive menos de 24 pra fazer o teste.
                </p>
            </div>
        </div>
    </body>
</html>
