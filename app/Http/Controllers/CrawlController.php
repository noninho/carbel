<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class CrawlController extends Controller
{
    // Url do site
    private $URL_BASE = 'https://seminovos.com.br/';

    /**
     * Busca os vaículos no site de acordo com os filtros passados.
     * 
     * 1 - Carrega o DOM do site
     * 2 - Pega todas as tags DIV
     * 3 - Filtra somente as DIV's que contem veículos
     * 4 - Itera nas div's e busca os dados dos veículos
     *
     * @param string $tipoVeiculo
     * 
     * @return json
     */
    public function crawl($tipoVeiculo)
    {
        // Url do site
        $url = $this->URL_BASE . $tipoVeiculo;

        /**
         * Filtros
         * 
         * [0] = tipo veículo
         * [1] = filtro1
         * [2] = filtro2
         * [3] = filtro3
         * [4] = filtro4
         * [5] = filtro5
         * [6] = filtro6
         */

        // Pega os parametros passados
        $params = \Route::current()->parameters();

        // Adiciona os filtros
        if(isset($params['filtro1'])) {
            $url = $url . '/' . $params['filtro1'];
        }
        
        if(isset($params['filtro2'])) {
            $url = $url . '/' . $params['filtro2'];
        }

        if(isset($params['filtro3'])) {
            $url = $url . '/' . $params['filtro3'];
        }

        if(isset($params['filtro4'])) {
            $url = $url . '/' . $params['filtro4'];
        }

        if(isset($params['filtro5'])) {
            $url = $url . '/' . $params['filtro5'];
        }
        
        if(isset($params['filtro6'])) {
            $url = $url . '/' . $params['filtro6'];
        }

        // Carrega o DOM
        $dom = new \DOMDocument('1.0');
        @$dom->loadHTMLFile($url);

        // Pega todas as divs do dom
        $divs = $dom->getElementsByTagName('div');

        // Filtra somente as DIV's dos carros
        foreach ($divs as $element) {
            $tipo = $element->getAttribute('itemtype');

            if(!$tipo || $tipo != 'http://schema.org/Car' || $tipo == '\n') {
                continue;
            }

            $divsCards[] = $element;
        }

        /**
         * Formato dos nodes
         * Div-card
         *      div.d-none schema-items
         *          [0] \n
         *          [1]meta-productID
         *          [2]span-sku
         *          [3]meta-url
         *          [4]span-bodyType
         *          [5]span-brand
         *          [6]span-model
         *          [7]span-name
         *          [8]span-description
         *          [9]span-mileageFromOdometer
         *              div-offers
         *                  span-price
         *                  meta-url
         *                  meta-priceCurrency
         *                  span-priceValidUntil  
         * 
         *      figure.col-12 col-md-3
         *          a
         *              img  
         * 
         *      div.card-content col-12 col-md-9 px-0
         */

        if (!isset($divsCards)) {
            return json_encode(['erro' => 'Veículos não encontrados com esse filtro.'], JSON_PRETTY_PRINT);
        }

        // Array de veículos
        $veiculos = [];
        
        // Contagem de veiculos
        $countVeiculos = 0;

        // Iterar nas div-card
        foreach ($divsCards as $card) {

            // Iterar nas div-items
            foreach ($card->childNodes as $item) {
                // Pula node '\n'
                if ($item->nodeName == '#text') continue;

                $tag    = $item->nodeName; // tag html
                $classe = $item->getAttribute('class'); // classe da div

                if ($tag == 'div' && $classe == 'd-none schema-items') {
                    // Itera nas propriedaes do veículo
                    foreach ($item->childNodes as $propItem) {
                        // Pula node '\n'
                        if ($propItem->nodeName == '#text') continue;

                        $prop = $propItem->getAttribute('itemprop');
                        
                        // Pega as propriedades do vaiculo
                        switch ($prop) {
                            case 'productID':
                                $veiculos[$countVeiculos]['productID'] = $propItem->getAttribute('content');
                                break;
                                
                            case 'sku':
                                $veiculos[$countVeiculos]['sku'] = $propItem->nodeValue;
                                break;
                                
                            case 'url':
                                $veiculos[$countVeiculos]['url'] = $propItem->getAttribute('content');
                                break;
                                
                            case 'bodyType':
                                $veiculos[$countVeiculos]['bodyType'] = $propItem->nodeValue;
                                break;
                                
                            case 'brand':
                                $veiculos[$countVeiculos]['brand'] = $propItem->nodeValue;
                                break;
                                
                            case 'model':
                                $veiculos[$countVeiculos]['model'] = $propItem->nodeValue;
                                break;
                                
                            case 'name':
                                $veiculos[$countVeiculos]['name'] = $propItem->nodeValue;
                                break;
                                
                            case 'description':
                                $veiculos[$countVeiculos]['description'] = $propItem->nodeValue;
                                break;
                                
                            case 'mileageFromOdometer':
                                $veiculos[$countVeiculos]['mileageFromOdometer'] = $propItem->nodeValue;
                                break;
                                
                            case 'offers':
                                // Itera nos itens
                                foreach ($propItem->childNodes as $offer) {
                                    // Pula node '\n'
                                    if ($offer->nodeName == '#text') continue;

                                    $propOffer = $offer->getAttribute('itemprop');

                                    switch ($propOffer) {
                                        case 'price':
                                            $veiculos[$countVeiculos]['price'] = $offer->nodeValue;
                                            break;
                                        
                                        case 'priceCurrency':
                                            $veiculos[$countVeiculos]['priceCurrency'] = $offer->getAttribute('content');
                                            break;
                                        
                                        case 'priceValidUntil':
                                            $veiculos[$countVeiculos]['priceValidUntil'] = $offer->nodeValue;
                                            break;
                                    }
                                }

                                break;
                                
                        }
                    }
                }

                // Imagem do veículo
                if ($tag == 'figure') {
                    // Itera nos nodes da tag figure
                    foreach ($item->childNodes as $figure) {
                        // Pula node '\n'
                        if ($figure->nodeName == '#text') continue;

                        // Tag <a>
                        $veiculos[$countVeiculos]['thumb'] = $figure->childNodes[1]->getAttribute('src');
                    }

                }

                // Card Info
                if ($tag == 'div' && $classe == 'card-content col-12 col-md-9 px-0') {
                    // Itera nos nodes do card-info
                    foreach ($item->childNodes as $cardContent) {
                        // Pula node '\n'
                        if ($cardContent->nodeName == '#text' || $cardContent->nodeName == 'a') continue;

                        $classeInfo = $cardContent->getAttribute('class');

                        // div .card-info
                        if ($classeInfo == 'card-info') {
                            // Itera nos itens do card-info
                            foreach ($cardContent->childNodes as $info) {
                                // Pula node '\n'
                                if ($info->nodeName == '#text') continue;

                                // Itera nos card-features
                                foreach ($info->childNodes as $feature) {
                                    // Pula node '\n'
                                    if ($feature->nodeName == '#text') continue;
                                    
                                    if ($feature->nodeName == 'p') {
                                        // Subtitle
                                        $veiculos[$countVeiculos]['subtitle'] = trim(preg_replace('/\s\s+/', ' ', $feature->nodeValue));
                                    }

                                    // Lista de recursos
                                    if ($feature->nodeName == 'ul' && $feature->getAttribute('class') == 'list-features') {
                                        foreach ($feature->childNodes as $li) {
                                            // Pula node '\n'
                                            if ($li->nodeName == '#text') continue;

                                            $title = $li->getAttribute('title');

                                            switch ($title) {
                                                case 'Ano de fabricação':
                                                    $veiculos[$countVeiculos]['anoFabricacao'] = trim(preg_replace('/\s\s+/', ' ', $li->childNodes[2]->nodeValue));
                                                    break;
                                                
                                                case 'Tipo de câmbio':
                                                    $veiculos[$countVeiculos]['cambio'] = trim(preg_replace('/\s\s+/', ' ', $li->childNodes[2]->nodeValue));
                                                    break;
                                            }

                                            
                                        }
                                    }

                                    // lista de opcionais
                                    if ($feature->nodeName == 'ul' && $feature->getAttribute('class') == 'list-inline') {
                                        $str = '';

                                        foreach ($feature->childNodes as $li) {
                                            // Pula node '\n'
                                            if ($li->nodeName == '#text') continue;

                                            $str .= trim(preg_replace('/\s\s+/', ' ', $li->nodeValue));
                                        }

                                        $veiculos[$countVeiculos]['optional'] = $str;
                                    }
                                        
                                    

                                }
                            }
                        }
                    }
                }
            }
            
            $countVeiculos++;
        }

        return '<pre>' . json_encode($veiculos, JSON_PRETTY_PRINT) .'</pre>';
    }
}
