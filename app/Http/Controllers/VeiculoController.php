<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class VeiculoController extends Controller
{
    // Url do site
    private $URL = 'https://seminovos.com.br/';

    /**
     * Buscar detalhes do veículo
     * 
     * @param string $slug
     * 
     * @return json
     */
    public function detalhesVeiculo($slug)
    {
        $urlCompleta = $this->URL . $slug;

        // Carrega o DOM
        $dom = new \DOMDocument('1.0');
        @$dom->loadHTMLFile($urlCompleta);

        // Pega todas as divs do dom
        $divs  = $dom->getElementsByTagName('div');
        $tagsP = $dom->getElementsByTagName('p');

        // Filtra somente a DIV's das informações e dos acessorios: item-info / full-features pt-print-0
        foreach ($divs as $element) {
            $class = $element->getAttribute('class');

            // Dados do veiculo
            if($class == 'item-info') {
                $dadosVeiculo['nome']           = $element->childNodes[1]->nodeValue;
                $dadosVeiculo['desc']           = trim(preg_replace('/\s\s+/', ' ', $element->childNodes[3]->nodeValue));
                $dadosVeiculo['preco']          = trim(preg_replace('/\s\s+/', ' ', $element->childNodes[5]->childNodes[2]->nodeValue));
                $dadosVeiculo['ano']            = $element->childNodes[7]->childNodes[1]->childNodes[2]->nodeValue;
                $dadosVeiculo['km']             = $element->childNodes[7]->childNodes[1]->childNodes[6]->nodeValue;
                $dadosVeiculo['cambio']         = $element->childNodes[7]->childNodes[1]->childNodes[10]->nodeValue;
                $dadosVeiculo['portas']         = $element->childNodes[7]->childNodes[1]->childNodes[14]->nodeValue;
                $dadosVeiculo['combustivel']    = $element->childNodes[7]->childNodes[1]->childNodes[18]->nodeValue;
                $dadosVeiculo['cor']            = $element->childNodes[7]->childNodes[1]->childNodes[22]->nodeValue;
                $dadosVeiculo['placa']          = $element->childNodes[7]->childNodes[1]->childNodes[26]->nodeValue;
                $dadosVeiculo['troca']          = $element->childNodes[7]->childNodes[1]->childNodes[30]->nodeValue;

            } 
            
            // Acessorios do veiculo
            else if ($class == 'full-features pt-print-0') {

                $acessorios = '';

                // Itera nas ul
                foreach ($element->childNodes as $ul) {
                    // Pula node '\n'
                    if ($ul->nodeName == '#text') continue;

                    if ($ul->getAttribute('class') == 'list-styled') {
                        // Itera nas li
                        foreach ($ul->childNodes as $li) {
                            // Pula node '\n'
                            if ($li->nodeName == '#text') continue;
                            
                            $acessorios .= trim(preg_replace('/\s\s+/', ' ', $li->nodeValue)) . ',';
                        }
                    }
                }

                $dadosVeiculo['acessorios'] = $acessorios;
            }
        }

        // Itera nas tags <p> para buscar as observações
        foreach ($tagsP as $p) {
            $class = $p->getAttribute('class');

            if($class == 'description-print') {
                $dadosVeiculo['observacoes'] = trim(preg_replace('/\s\s+/', ' ', $p->nodeValue));
            }
        }

        // Retorna se o veículo não existir
        if (!isset($dadosVeiculo)) {
            return json_encode(['erro' => 'Veículo não encontrado'], JSON_PRETTY_PRINT);
        }

        return '<pre>' . json_encode($dadosVeiculo, JSON_PRETTY_PRINT) . '</pre>';
    }
}
